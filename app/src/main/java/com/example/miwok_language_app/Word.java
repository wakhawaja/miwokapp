package com.example.miwok_language_app;

public class Word {

    /**
     * Default translation for the word
     */
    private String mDefaultTranslation;

    /**
     * Miwok translation for the word
     */
    private String mMiwokTranslation;

    /**
     * Miwok image id for the word
     */
    private int mImageResourceId = NO_IMAGE_PROVIDED;

    private static final int NO_IMAGE_PROVIDED = -1;

    private int mediaPlayerResourceId;

    /**
     * Create a new Word object.
     *
     * @param defaultTranslation is the word in a language that the user is already familiar with
     *                           (such as English)
     * @param miwokTranslation   is the word in the Miwok language
     */

    public Word(int ImgResId, String defaultTranslation, String miwokTranslation, int mp) {
        setDefaultTranslation(defaultTranslation);
        setMiwokTranslation(miwokTranslation);
        setMimageResourceId(ImgResId);
        setMediaPlayer(mp);
    }

    public Word(String defaultTranslation, String miwokTranslation, int mp) {
        setDefaultTranslation(defaultTranslation);
        setMiwokTranslation(miwokTranslation);
        setMediaPlayer(mp);
    }

    private void setDefaultTranslation(String DefaultTransl) {
        mDefaultTranslation = DefaultTransl;
    }

    private void setMiwokTranslation(String MiwokTransl) {
        mMiwokTranslation = MiwokTransl;
    }

    private void setMimageResourceId(int imageResourceId) {
        mImageResourceId = imageResourceId;
    }

    private void setMediaPlayer(int mp3) {
        mediaPlayerResourceId = mp3;
    }

    /**
     * Get the default translation of the word.
     */
    public String getDefaultTranslation() {
        return mDefaultTranslation;
    }

    /**
     * Get the Miwok translation of the word.
     */
    public String getMiwokTranslation() {
        return mMiwokTranslation;
    }

    /**
     * Get the Image Resource Id of the word.
     */
    public int getMimageResourceId() {
        return mImageResourceId;
    }

    /**
     * Returns Wethere or not there is image for this word
     */
    public boolean hasimage() {
        return mImageResourceId != NO_IMAGE_PROVIDED;
    }

    /**
     * Get the Image Resource Id of the word.
     */
    public int getMediaPlayer() {
        return mediaPlayerResourceId;
    }

    @Override
    public String toString() {
        return "Word{" +
                "mDefaultTranslation='" + mDefaultTranslation + '\'' +
                ", mMiwokTranslation='" + mMiwokTranslation + '\'' +
                ", mImageResourceId=" + mImageResourceId +
                ", mediaPlayerResourceId=" + mediaPlayerResourceId +
                '}';
    }
}